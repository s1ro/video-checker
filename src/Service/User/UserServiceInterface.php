<?php

namespace App\Service\User;

use App\Entity\User;
use App\Service\User\Exception\UserAlreadyExistsException;

interface UserServiceInterface
{
    /**
     * @throws UserAlreadyExistsException
     */
    public function createUser(string $email, string $password): User;
}