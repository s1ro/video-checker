<?php

namespace App\Service\User;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\User\Exception\UserAlreadyExistsException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService implements UserServiceInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private UserPasswordHasherInterface $passwordHasher
    ) {}

    /**
     * @inheritDoc
     */
    public function createUser(string $email, string $password): User
    {
        $exists = $this->userRepository->findOneBy(['email' => $email]);
        if (!is_null($exists)) {
            throw UserAlreadyExistsException::create($email);
        }
        $user = new User();
        $user
            ->setEmail($email)
            ->setPassword($this->passwordHasher->hashPassword($user, $password));
        $this->userRepository->save($user, true);
        return $user;
    }
}