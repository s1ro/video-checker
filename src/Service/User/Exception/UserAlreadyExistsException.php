<?php

namespace App\Service\User\Exception;

class UserAlreadyExistsException extends \Exception
{
    public static function create(string $email): static
    {
        return new static("User with $email already exists");
    }
}