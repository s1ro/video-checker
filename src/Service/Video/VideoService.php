<?php

namespace App\Service\Video;

use App\Entity\Video;
use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\Message\VideoUploadedMessage;
use App\Service\Video\Infrastructure\Moderation\DTO\SightEngineFrameDTO;
use Symfony\Component\Messenger\MessageBusInterface;

class VideoService implements VideoServiceInterface
{
    public function __construct(
        private VideoRepository $videoRepository,
        private MessageBusInterface $bus
    ) {}

    public function save(Video $video): void
    {
        $this->videoRepository->save($video, true);
        $this->bus->dispatch(new VideoUploadedMessage($video->getId()));
    }

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        return $this->videoRepository->findAll();
    }

    /**
     * @inheritDoc
     */
    public function setSightEngineFrames(Video $video, array $frames): Video
    {
        $video->setSightEngineResult($frames);
        $this->videoRepository->save($video, true);
        $this->calculateSightEngineStatus($video);
        return $video;
    }

    public function calculateSightEngineStatus(Video $video): void
    {
        if (empty($video->getSightEngineResult())) {
            return;
        }
        $video->setSightEngineStatus(Video::SIGHT_ENGINE_STATUS_CONFIRMED);
        frame_loop:
        foreach ($video->getSightEngineResult() as $frame) {
            foreach ($frame as $key => $item) {
                if ($key !== 'position' && $item > 0.5) {
                    $video->setSightEngineStatus(Video::STATUS_DECLINED);
                    break 2;
                }
            }
        }
        $this->videoRepository->save($video, true);
    }
}