<?php

namespace App\Service\Video\Infrastructure\Moderation\DTO\Webhook;

use App\Service\Video\Infrastructure\Moderation\DTO\SightEngineFrameDTO;
use App\Service\Video\Infrastructure\Moderation\DTO\Webhook\Exception\SightEngineWebhookDTOException;
use Symfony\Component\HttpFoundation\Request;

class SightEngineWebhookDTO
{
    public const STATUS_FINISHED = 'finished';

    public function __construct(
        public readonly string $mediaId,
        public readonly string $status,
        public readonly array $frameDTOs
    ) {}

    /**
     * @throws SightEngineWebhookDTOException
     */
    public static function fromRequest(Request $request): static
    {
        $data = json_decode($request->getContent(), true);
        if (!isset($data['media']['id'], $data['data']['status'], $data['data']['frames'])) {
            throw SightEngineWebhookDTOException::createInvalidRequest();
        }
        $frames = [];
        foreach ($data['data']['frames'] as $frame) {
            if (!isset($frame['info']['position'])) {
                continue;
            }
            $frames[] = new SightEngineFrameDTO(
                $frame['info']['position'],
                $frame['nudity']['sexual_activity'] ?? null,
                $frame['nudity']['sexual_display'] ?? null,
                $frame['nudity']['erotica'] ?? null,
                $frame['nudity']['suggestive'] ?? null,
                $frame['weapon'] ?? null,
                $frame['alcohol'] ?? null,
                $frame['drugs'] ?? null,
                $frame['gore']['prob'] ?? null,
                $frame['offensive']['nazi'] ?? null,
                $frame['offensive']['confederate'] ?? null,
                $frame['offensive']['supremacist'] ?? null,
                $frame['offensive']['terrorist'] ?? null,
                $frame['offensive']['middle_finger'] ?? null,
            );
        }
        return new static(
            $data['media']['id'],
            $data['data']['status'],
            $frames
        );
    }
}