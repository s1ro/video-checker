<?php

namespace App\Service\Video\Infrastructure\Moderation\DTO\Webhook\Exception;

class SightEngineWebhookDTOException extends \Exception
{
    public static function createInvalidRequest(): static
    {
        return new static('Invalid webhook request!');
    }
}