<?php

namespace App\Service\Video\Infrastructure\Moderation\DTO;

class SightEngineFrameDTO implements \JsonSerializable
{
    public function __construct(
        public readonly int $position,
        public readonly ?float $sexualActivity,
        public readonly ?float $sexualDisplay,
        public readonly ?float $erotica,
        public readonly ?float $suggestive,
        public readonly ?float $weapon,
        public readonly ?float $alcohol,
        public readonly ?float $drugs,
        public readonly ?float $gore,
        public readonly ?float $nazi,
        public readonly ?float $confederate,
        public readonly ?float $supremacist,
        public readonly ?float $terrorist,
        public readonly ?float $middleFinger
    ) {}

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): array
    {
        return [
            'position' => $this->position,
            'sexual_activity' => $this->sexualActivity,
            'sexual_display' => $this->sexualDisplay,
            'erotica' => $this->erotica,
            'suggestive' => $this->suggestive,
            'weapon' => $this->weapon,
            'alcohol' => $this->alcohol,
            'drugs' => $this->drugs,
            'gore' => $this->gore,
            'nazi' => $this->nazi,
            'confederate' => $this->confederate,
            'supremacist' => $this->supremacist,
            'terrorist' => $this->terrorist,
            'middle_finger' => $this->middleFinger,
        ];
    }
}