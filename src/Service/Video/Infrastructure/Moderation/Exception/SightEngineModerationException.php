<?php

namespace App\Service\Video\Infrastructure\Moderation\Exception;

class SightEngineModerationException extends \Exception
{
    public static function create(): static
    {
        return new static('SightEngine moderation error');
    }
}