<?php

namespace App\Service\Video\Infrastructure\Moderation\Exception;

class GoogleVideoIntelligenceModerationException extends \Exception
{
    public static function fromThrowable(\Throwable $t): static
    {
        return new static($t->getMessage(), $t->getCode(), $t);
    }
}