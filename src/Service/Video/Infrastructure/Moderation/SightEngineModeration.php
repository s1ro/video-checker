<?php

namespace App\Service\Video\Infrastructure\Moderation;

use App\Entity\Video;
use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\Moderation\Exception\SightEngineModerationException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SightEngineModeration
{
    private const MAX_FILE_SIZE = 50 * 1024 * 1024;
    private const MODELS = ['nudity-2.0','wad','gore','offensive'];
    private const URL = 'https://api.sightengine.com/1.0/video/check.json';

    public function __construct(
        private string $videoDir,
        private string $sightEngineUser,
        private string $sightEngineSecret,
        private VideoRepository $videoRepository,
        private UrlGeneratorInterface $urlGenerator
    ) {}

    /**
     * @throws SightEngineModerationException
     */
    public function moderate(Video $video): void
    {
        $video->setSightEngineCanBeModerated($this->checkIfCanBeModerated($video));
        $this->videoRepository->save($video, true);
        if (!$video->isSightEngineCanBeModerated()) {
            return;
        }

        $params = [
            'media' => new \CURLFile($this->videoDir . $video->getPath()),
            'models' => implode(',', self::MODELS),
            'callback_url' => $this->urlGenerator->generate('sight_engine_webhook', [
                'id' => $video->getId(),
            ], UrlGeneratorInterface::ABSOLUTE_URL),
            'api_user' => $this->sightEngineUser,
            'api_secret' => $this->sightEngineSecret,
        ];

        $ch = curl_init(self::URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $response = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($response, true);
        if (!isset($data['media']['id'])) {
            throw SightEngineModerationException::create();
        }
        $video->setSightEngineId($data['media']['id']);
        $this->videoRepository->save($video, true);
    }

    private function checkIfCanBeModerated(Video $video): bool
    {
        if (filesize($this->videoDir . $video->getPath()) > self::MAX_FILE_SIZE) {
            return false;
        }
        return true;
    }
}