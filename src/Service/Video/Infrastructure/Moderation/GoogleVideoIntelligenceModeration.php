<?php

namespace App\Service\Video\Infrastructure\Moderation;

use App\Entity\Video;
use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\Moderation\Exception\GoogleVideoIntelligenceModerationException;
use Google\Cloud\VideoIntelligence\V1\AnnotateVideoResponse;
use Google\Cloud\VideoIntelligence\V1\ExplicitContentAnnotation;
use Google\Cloud\VideoIntelligence\V1\ExplicitContentFrame;
use Google\Cloud\VideoIntelligence\V1\Feature;
use Google\Cloud\VideoIntelligence\V1\VideoAnnotationResults;
use Google\Cloud\VideoIntelligence\V1\VideoIntelligenceServiceClient;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class GoogleVideoIntelligenceModeration
{
    public function __construct(
        #[Autowire('%env(GOOGLE_CLOUD_STORAGE_BUCKET_NAME)%')] private string $backetName,
        private string $googleKeyFile,
        private VideoRepository $videoRepository
    ) {}

    /**
     * @throws GoogleVideoIntelligenceModerationException
     */
    public function moderate(Video $video): void
    {
        try {
            $intl = new VideoIntelligenceServiceClient([
                'credentials' => $this->googleKeyFile,
            ]);
            $inputUri = 'gs://' . $this->backetName . '/' . $video->getPath();
            $features = [Feature::EXPLICIT_CONTENT_DETECTION];

            $operation = $intl->annotateVideo([
                'inputUri' => $inputUri,
                'features' => $features
            ]);

            $operation->pollUntilComplete();

            if ($operation->operationSucceeded()) {
                /** @var AnnotateVideoResponse $response */
                $response = $operation->getResult();
                $found = false;
                $pFound = false;
                $data = [];
                /** @var VideoAnnotationResults $result */
                foreach ($response->getAnnotationResults() as $result) {
                    foreach ($result->getExplicitAnnotation()?->getFrames() as $frame) {
                        $timeOffset = $frame->getTimeOffset()->getSeconds();
                        $likelihood = $frame->getPornographyLikelihood();
                        $data[] = ['time_offset' => $timeOffset, 'likelihood' => $likelihood];
                        if ($frame->getPornographyLikelihood() === 5) {
                            $found = true;
                        }
                        if ($frame->getPornographyLikelihood() === 4 || $frame->getPornographyLikelihood() === 3) {
                            $pFound = true;
                        }
                    }
                }
                $video->setGoogleModerationResult($data);
                if ($found) {
                    $video->setStatus(Video::STATUS_DECLINED);
                } elseif ($pFound) {
                    $video->setStatus(Video::STATUS_PROBABLY);
                } else {
                    $video->setStatus(Video::STATUS_CONFIRMED);
                }
                $this->videoRepository->save($video, true);
            }
        } catch (\Throwable $t) {
            throw GoogleVideoIntelligenceModerationException::fromThrowable($t);
        }
    }
}