<?php

namespace App\Service\Video\Infrastructure\Form;

use App\Entity\Video;
use App\Service\Video\Infrastructure\FileUpload\VideoFileUploader;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class VideoFormType extends AbstractType
{
    public function __construct(private VideoFileUploader $fileUploader) {}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'required' => true,
                'constraints' => [new Assert\NotBlank()]
            ])
            ->add('path', FileType::class, [
                'required' => true,
                'attr' => ['accept' => 'video/*, application/x-mpegURL'],
                'constraints' => [
                    new Assert\File([
                        'mimeTypes' => [
                            'video/x-flv',
                            'video/mp4',
                            'application/x-mpegURL',
                            'video/MP2T',
                            'video/3gpp',
                            'video/quicktime',
                            'video/x-msvideo',
                            'video/x-ms-wmv'
                        ]
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Video::class,
        ]);
    }
}