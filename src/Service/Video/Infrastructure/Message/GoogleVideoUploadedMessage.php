<?php

namespace App\Service\Video\Infrastructure\Message;

class GoogleVideoUploadedMessage
{
    public function __construct(public readonly int $id) {}
}