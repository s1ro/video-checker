<?php

namespace App\Service\Video\Infrastructure\Message;

class VideoUploadedMessage
{
    public function __construct(public readonly int $id) {}
}