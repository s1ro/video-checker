<?php

namespace App\Service\Video\Infrastructure\FileUpload;

use App\Entity\Video;
use App\Service\Video\Infrastructure\FileUpload\Exception\GoogleCloudStorageUploadException;
use Google\Cloud\Core\Exception\GoogleException;
use Google\Cloud\Storage\StorageClient;

class GoogleCloudStorageFileUploader
{
    public function __construct(
        private string $bucketName,
        private string $googleKeyFile,
        private string $videoDir
    ) {}

    /**
     * @throws GoogleCloudStorageUploadException
     */
    public function upload(Video $video): void
    {
        try {
            $storage = new StorageClient([
                'keyFilePath' => $this->googleKeyFile,
            ]);
            $bucket = $storage->bucket($this->bucketName);
            if (!$bucket->exists()) {
                $storage->createBucket($this->bucketName);
            }
            $chunkSize = 262144;
            $options = ['chunkSize' => $chunkSize, 'predefinedAcl' => 'publicRead'];

            $uploader = $bucket->getResumableUploader(
                fopen($this->videoDir . $video->getPath(), 'r'),
                $options
            );
            try {
                $uploader->upload();
            } catch (GoogleException $e) {
                $resumeUri = $uploader->getResumeUri();
                $uploader->resume($resumeUri);
            }
        } catch (\Throwable $t) {
            throw GoogleCloudStorageUploadException::fromThrowable($t);
        }
    }
}