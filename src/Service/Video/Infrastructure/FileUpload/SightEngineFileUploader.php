<?php

namespace App\Service\Video\Infrastructure\FileUpload;

use App\Entity\Video;
use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\FileUpload\Exception\SightEngineFileUploaderException;
use Psr\Log\LoggerInterface;

class SightEngineFileUploader
{
    public function __construct(
        private string $videoDir,
        private VideoRepository $videoRepository,
        private string $sightEngineUser,
        private string $sightEngineSecret,
        private LoggerInterface $logger
    ) {}

    /**
     * @throws SightEngineFileUploaderException
     */
    public function upload(Video $video): void
    {
        $uploadUrl = $this->getUploadUrl();

        $fh_res = fopen($this->videoDir . $video->getPath(), 'r');

        $ch = curl_init($uploadUrl);
        curl_setopt($ch, CURLOPT_PUT, true);
        curl_setopt($ch, CURLOPT_INFILE, $fh_res);
        curl_setopt($ch, CURLOPT_INFILESIZE, filesize($this->videoDir . $video->getPath()));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $response = curl_exec($ch);
        curl_close($ch);

        $this->logger->cri('SightEngine upload res', [
            'response' => $response,
        ]);
    }

    /**
     * @throws SightEngineFileUploaderException
     */
    private function getUploadUrl(): string
    {
        $params = [
            'api_user' => $this->sightEngineUser,
            'api_secret' => $this->sightEngineSecret,
        ];

        $ch = curl_init('https://api.sightengine.com/1.0/upload/create-video.json?' . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $this->logger->critical('SightEngine Upload Url Response', ['response' => $response]);
        curl_close($ch);

        if ($response === false) {
            throw SightEngineFileUploaderException::createGenerateError();
        }

        $data = json_decode($response, true);

        if (!isset($data['upload']['url'])) {
            throw SightEngineFileUploaderException::createGenerateError();
        }

        return $data['upload']['url'];
    }
}