<?php

namespace App\Service\Video\Infrastructure\FileUpload\Exception;

class SightEngineFileUploaderException extends \Exception
{
    public static function createGenerateError(): static
    {
        return new static('SightEngine Generate Upload Url Error!');
    }
}