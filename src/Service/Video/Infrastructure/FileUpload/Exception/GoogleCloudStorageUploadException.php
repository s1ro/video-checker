<?php

namespace App\Service\Video\Infrastructure\FileUpload\Exception;

class GoogleCloudStorageUploadException extends \Exception
{
    public static function fromThrowable(\Throwable $t): static
    {
        return new static($t->getMessage(), $t->getCode(), $t);
    }
}