<?php

namespace App\Service\Video\Infrastructure\MessageHandler;

use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\Message\VideoUploadedMessage;
use App\Service\Video\Infrastructure\Moderation\Exception\SightEngineModerationException;
use App\Service\Video\Infrastructure\Moderation\SightEngineModeration;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler(handles: VideoUploadedMessage::class)]
class SightEngineModerationHandler
{
    public function __construct(
        private VideoRepository $videoRepository,
        private SightEngineModeration $sightEngineModeration,
        private LoggerInterface $logger
    ) {}

    /**
     * @throws \Throwable
     * @throws SightEngineModerationException
     */
    public function __invoke(VideoUploadedMessage $message): void
    {
        $video = $this->videoRepository->find($message->id);
        try {
            $this->sightEngineModeration->moderate($video);
        } catch (\Throwable $t) {
            $this->logger->error('SightEngine Moderation Error!', [
                'message' => $t->getMessage(),
                'code' => $t->getCode(),
            ]);
            throw $t;
        }
    }
}