<?php

namespace App\Service\Video\Infrastructure\MessageHandler;

use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\FileUpload\Exception\SightEngineFileUploaderException;
use App\Service\Video\Infrastructure\FileUpload\SightEngineFileUploader;
use App\Service\Video\Infrastructure\Message\VideoUploadedMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

//#[AsMessageHandler(handles: VideoUploadedMessage::class)]
class SightEngineUploaderMessageHandler
{
    public function __construct(
        private VideoRepository $videoRepository,
        private SightEngineFileUploader $fileUploader,
        private LoggerInterface $logger
    ) {}

    /**
     * @throws SightEngineFileUploaderException
     * @throws \Throwable
     */
    public function __invoke(VideoUploadedMessage $message): void
    {
        $video = $this->videoRepository->find($message->id);

        try {
            $this->fileUploader->upload($video);
        } catch (\Throwable $t) {
            $this->logger->error('SightEngine File Upload Error', [
                'message' => $t->getMessage(),
                'code' => $t->getCode(),
            ]);
            throw $t;
        }
    }
}