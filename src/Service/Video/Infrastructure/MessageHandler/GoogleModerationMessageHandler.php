<?php

namespace App\Service\Video\Infrastructure\MessageHandler;

use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\Message\GoogleVideoUploadedMessage;
use App\Service\Video\Infrastructure\Moderation\Exception\GoogleVideoIntelligenceModerationException;
use App\Service\Video\Infrastructure\Moderation\GoogleVideoIntelligenceModeration;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler(handles: GoogleVideoUploadedMessage::class)]
class GoogleModerationMessageHandler
{
    public function __construct(
        private VideoRepository $videoRepository,
        private GoogleVideoIntelligenceModeration $moderation,
        private LoggerInterface $logger
    ) {}

    public function __invoke(GoogleVideoUploadedMessage $message): void
    {
        $video = $this->videoRepository->find($message->id);
        if (is_null($video)) {
            throw new \RuntimeException('Video not found!');
        }
        try {
            $this->moderation->moderate($video);
        } catch (GoogleVideoIntelligenceModerationException $e) {
            $this->logger->error('Google Video Moderation Error', [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
            throw $e;
        }
        $this->logger->info('Video uploaded [' . $video->getId() . ']');
    }
}