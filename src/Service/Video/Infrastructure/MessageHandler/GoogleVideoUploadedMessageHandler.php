<?php

namespace App\Service\Video\Infrastructure\MessageHandler;

use App\Repository\VideoRepository;
use App\Service\Video\Infrastructure\FileUpload\Exception\GoogleCloudStorageUploadException;
use App\Service\Video\Infrastructure\FileUpload\GoogleCloudStorageFileUploader;
use App\Service\Video\Infrastructure\Message\GoogleVideoUploadedMessage;
use App\Service\Video\Infrastructure\Message\VideoUploadedMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler(handles: VideoUploadedMessage::class)]
class GoogleVideoUploadedMessageHandler
{
    public function __construct(
        private VideoRepository $videoRepository,
        private GoogleCloudStorageFileUploader $uploader,
        private LoggerInterface $logger,
        private MessageBusInterface $bus
    ) {}

    /**
     * @throws GoogleCloudStorageUploadException
     */
    public function __invoke(VideoUploadedMessage $message): void
    {
        $video = $this->videoRepository->find($message->id);
        if (is_null($video)) {
            throw new \RuntimeException('Video not found!');
        }
        try {
            $this->uploader->upload($video);
            $this->bus->dispatch(new GoogleVideoUploadedMessage($video->getId()));
        } catch (GoogleCloudStorageUploadException $e) {
            $this->logger->error('Google upload Error!', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ]);
            throw $e;
        }
        $this->logger->info('Video uploaded [' . $video->getId() . ']');
    }
}