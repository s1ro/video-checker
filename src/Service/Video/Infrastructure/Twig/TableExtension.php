<?php

namespace App\Service\Video\Infrastructure\Twig;

use App\Entity\Video;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TableExtension extends AbstractExtension
{
    /**
     * @inheritDoc
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('video_get_main_table_row_class', $this->getMainTableRowClass(...)),
            new TwigFunction('video_get_sight_engine_table_row_class', $this->getSightEngineTableRowClass(...)),
        ];
    }

    public function getMainTableRowClass(Video $video): ?string
    {
        if (
            ($video->getStatus() === Video::STATUS_CONFIRMED
            &&
            ($video->getSightEngineStatus() === Video::SIGHT_ENGINE_STATUS_CONFIRMED))
            ||
            (!$video->isSightEngineCanBeModerated() && $video->getStatus() === Video::STATUS_CONFIRMED)
        ) {
            return 'table-success';
        }

        if (
            ($video->getStatus() === Video::STATUS_DECLINED
            ||
            ($video->getSightEngineStatus() === Video::SIGHT_ENGINE_STATUS_DECLINED))
            ||
            (!$video->isSightEngineCanBeModerated() && $video->getStatus() === Video::SIGHT_ENGINE_STATUS_DECLINED)
        ) {
            return 'table-danger';
        }

        if ($video->getStatus() === Video::STATUS_PROBABLY) {
            return 'table-warning';
        }

        return null;
    }

    public function getSightEngineTableRowClass(array $frame): ?string
    {
        foreach ($frame as $key => $value) {
            if ($key !== 'position' && $value > 0.5) {
                return 'table-danger';
            }
        }
        return null;
    }
}