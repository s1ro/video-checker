<?php

namespace App\Service\Video;

use App\Entity\Video;
use App\Service\Video\Infrastructure\Moderation\DTO\SightEngineFrameDTO;

interface VideoServiceInterface
{
    public function save(Video $video): void;

    /**
     * @return Video[]
     */
    public function getAll(): array;

    /**
     * @param Video $video
     * @param SightEngineFrameDTO[] $frames
     * @return Video
     */
    public function setSightEngineFrames(Video $video, array $frames): Video;

    public function calculateSightEngineStatus(Video $video): void;
}