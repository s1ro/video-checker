<?php

namespace App\Command;

use App\Repository\VideoRepository;
use App\Service\Video\VideoServiceInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:video:calculate-sight-engine-status',
    description: 'Calculates Sight Engine status',
)]
class VideoCalculateSightEngineStatusCommand extends Command
{
    private VideoRepository $videoRepository;
    private VideoServiceInterface $videoService;

    public function __construct(
        VideoRepository $videoRepository,
        VideoServiceInterface $videoService,
        string $name = null
    )
    {
        $this->videoRepository = $videoRepository;
        $this->videoService = $videoService;
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Calculate Sight Engine status');
        $io->note('Fetching videos');
        $videos = $this->videoRepository->findAll();

        foreach ($videos as $video) {
            $io->note('Calculating for video [' . $video->getId() . ']');
            $this->videoService->calculateSightEngineStatus($video);
        }

        $io->success('Success!');

        return Command::SUCCESS;
    }
}
