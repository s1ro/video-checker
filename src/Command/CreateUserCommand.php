<?php

namespace App\Command;

use App\Service\User\Exception\UserAlreadyExistsException;
use App\Service\User\UserServiceInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create-user',
    description: 'Creates user',
)]
class CreateUserCommand extends Command
{
    private UserServiceInterface $userService;

    public function __construct(UserServiceInterface $userService, string $name = null)
    {
        $this->userService = $userService;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'User`s email')
            ->addArgument('password', InputArgument::REQUIRED, 'User`s password')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $email = $input->getArgument('email');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $io->error("$email is not valid email");
            return Command::FAILURE;
        }

        try {
            $this->userService->createUser($email, $input->getArgument('password'));
        } catch (UserAlreadyExistsException $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }

        $io->success('Success!');

        return Command::SUCCESS;
    }
}
