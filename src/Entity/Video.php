<?php

namespace App\Entity;

use App\Repository\VideoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VideoRepository::class)]
class Video
{
    public const STATUS_CREATED = 'created';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_DECLINED = 'declined';
    public const STATUS_PROBABLY = 'probably';

    public const SIGHT_ENGINE_STATUS_CREATED = 'created';
    public const SIGHT_ENGINE_STATUS_CONFIRMED = 'confirmed';
    public const SIGHT_ENGINE_STATUS_DECLINED = 'declined';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $path = null;

    #[ORM\Column(length: 255, options: ['default' => self::STATUS_CREATED])]
    private ?string $status = self::STATUS_CREATED;

    #[ORM\Column(nullable: true, options: ['default' => '[]'])]
    private array $googleModerationResult = [];

    #[ORM\Column(nullable: true, options: ['default' => null])]
    private ?bool $sightEngineCanBeModerated = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $sightEngineId = null;

    #[ORM\Column(nullable: true, options: ['default' => null])]
    private array $sightEngineResult = [];

    #[ORM\Column(length: 255, options: ['default' => self::STATUS_CREATED])]
    private ?string $sightEngineStatus = self::STATUS_CREATED;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getGoogleModerationResult(): array
    {
        return $this->googleModerationResult;
    }

    public function setGoogleModerationResult(?array $googleModerationResult): self
    {
        $this->googleModerationResult = $googleModerationResult;

        return $this;
    }

    public function isSightEngineCanBeModerated(): ?bool
    {
        return $this->sightEngineCanBeModerated;
    }

    public function setSightEngineCanBeModerated(bool $sightEngineCanBeModerated): self
    {
        $this->sightEngineCanBeModerated = $sightEngineCanBeModerated;

        return $this;
    }

    public function getSightEngineId(): ?string
    {
        return $this->sightEngineId;
    }

    public function setSightEngineId(?string $sightEngineId): self
    {
        $this->sightEngineId = $sightEngineId;

        return $this;
    }

    public function getSightEngineResult(): array
    {
        return $this->sightEngineResult;
    }

    public function setSightEngineResult(?array $sightEngineResult): self
    {
        $this->sightEngineResult = $sightEngineResult;

        return $this;
    }

    public function getSightEngineStatus(): ?string
    {
        return $this->sightEngineStatus;
    }

    public function setSightEngineStatus(string $sightEngineStatus): self
    {
        $this->sightEngineStatus = $sightEngineStatus;

        return $this;
    }
}
