<?php

namespace App\Controller\Webhook;

use App\Entity\Video;
use App\Service\Video\Infrastructure\Moderation\DTO\Webhook\Exception\SightEngineWebhookDTOException;
use App\Service\Video\Infrastructure\Moderation\DTO\Webhook\SightEngineWebhookDTO;
use App\Service\Video\VideoServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/sight-engine/webhook')]
class SightEngineWebhookController extends AbstractController
{
    #[Route('/video/{id}', name: 'sight_engine_webhook')]
    public function handle(
        Video $video,
        Request $request,
        VideoServiceInterface $videoService
    ): Response
    {
        try {
            $requestDTO = SightEngineWebhookDTO::fromRequest($request);
        } catch (SightEngineWebhookDTOException) {
            return new Response(status: Response::HTTP_BAD_REQUEST);
        }

        if ($requestDTO->status === SightEngineWebhookDTO::STATUS_FINISHED) {
            $videoService->setSightEngineFrames($video, $requestDTO->frameDTOs);
        }

        return new Response();
    }
}