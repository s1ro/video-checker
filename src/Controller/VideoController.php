<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Video;
use App\Service\Video\Infrastructure\FileUpload\GoogleCloudStorageFileUploader;
use App\Service\Video\Infrastructure\FileUpload\VideoFileUploader;
use App\Service\Video\Infrastructure\Form\VideoFormType;
use App\Service\Video\Infrastructure\Moderation\GoogleVideoIntelligenceModeration;
use App\Service\Video\VideoServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/video')]
#[IsGranted(User::ROLE_USER)]
class VideoController extends AbstractController
{
    #[Route('/', name: 'video_index', methods: ['GET'])]
    public function index(VideoServiceInterface $videoService): Response
    {
        return $this->render('video/index.html.twig', [
            'videos' => $videoService->getAll(),
        ]);
    }

    #[Route('/new', name: 'video_new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        VideoServiceInterface $videoService,
        VideoFileUploader $fileUploader
    ): Response
    {
        $video = new Video();
        $form = $this->createForm(VideoFormType::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $video->setPath($fileUploader->upload($form->get('path')->getData()));
            $videoService->save($video);
            return $this->redirectToRoute('video_index');
        }

        return $this->render('video/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/', name: 'video_view', methods: ['GET'])]
    public function view(Video $video): Response
    {
        return $this->render('video/view.html.twig', [
            'video' => $video,
        ]);
    }
}