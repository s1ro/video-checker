<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230311144923 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE video ADD sight_engine_status VARCHAR(255) DEFAULT \'created\' NOT NULL');
    }

    public function postUp(Schema $schema): void
    {
        $php = (new PhpExecutableFinder())->find();
        $process = new Process([$php, dirname(__DIR__) . '/bin/console', 'app:video:calculate-sight-engine-status']);
        $process->start();
        foreach ($process->getIterator(Process::ITER_SKIP_ERR | Process::ITER_KEEP_OUTPUT) as $data) {
            echo $data;
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE video DROP sight_engine_status');
    }
}
